import sys
from datetime import timedelta, datetime
from random import randint
from typing import List

from pyspark.shell import sqlContext
from pyspark.sql import SparkSession, DataFrame, Window

import pyspark.sql.functions as f
from utils import *

from pyspark.sql.types import StructField, IntegerType, StructType, StringType, FloatType, DateType, DoubleType

magic_percentile = f.expr('percentile_approx(TRANAMOUNT, 0.5)')


def divide(n, m):
    if n == float(0):
        return 0
    if m == float(0):
        return 0
    return n/m


def random_num(a):
    return randint(0, 1)


division_udf = f.udf(divide)
random_udf = f.udf(random_num)


def metrics_df(df: DataFrame):
    # w is for debit and d is for credit
    # n is for number and a is for sum
    # c is for cash transactions, t is for online transfers, f is for forex
    # m1,m6,m12 is for 1,6,12 months
    # med is for median, max is for maximum

    max_tran_date = get_max_tran_date(df)
    one_month_before = max_tran_date - timedelta(days=30)
    six_months_before = max_tran_date - timedelta(days=180)
    twelve_months_before = max_tran_date - timedelta(days=365)

    twelve_mth_df = df.filter(transaction_df.TRANDATE > twelve_months_before)
    twelve_mth_w_df = twelve_mth_df.filter(df.PARTTRANTYPE == "D")
    twelve_mth_d_df = twelve_mth_df.filter(df.PARTTRANTYPE == "C")

    six_mth_df = df.filter(twelve_mth_df.TRANDATE > six_months_before)
    six_mth_w_df = six_mth_df.filter(df.PARTTRANTYPE == "D")
    six_mth_d_df = six_mth_df.filter(df.PARTTRANTYPE == "C")

    one_mth_df = df.filter(six_mth_df.TRANDATE > one_month_before)
    one_mth_w_df = one_mth_df.filter(df.PARTTRANTYPE == "D")
    one_mth_d_df = one_mth_df.filter(df.PARTTRANTYPE == "C")

    distinct_account_numbers_df = twelve_mth_df.select(df.SOURCE_ACCOUNT_NBR).distinct()
    twelve_months_metrics = calculate_months_metrics(distinct_account_numbers_df, twelve_mth_w_df, "w", "m12")
    twelve_months_metrics = twelve_months_metrics.join(
        calculate_months_metrics(distinct_account_numbers_df, twelve_mth_d_df, "d", "m12"),
        on=["SOURCE_ACCOUNT_NBR"], how='left')

    six_months_metrics = calculate_months_metrics(distinct_account_numbers_df, six_mth_w_df, "w", "m6")
    six_months_metrics = six_months_metrics.join(
        calculate_months_metrics(distinct_account_numbers_df, six_mth_d_df, "d", "m6"),
        on=["SOURCE_ACCOUNT_NBR"], how='left')

    one_month_metrics = calculate_months_metrics(distinct_account_numbers_df, one_mth_w_df, "w", "m1")
    one_month_metrics = one_month_metrics.join(
        calculate_months_metrics(distinct_account_numbers_df,one_mth_d_df, "d", "m1"),
        on=["SOURCE_ACCOUNT_NBR"], how='left')

    sixty_col_df = one_month_metrics.join(six_months_metrics.join(twelve_months_metrics, on=["SOURCE_ACCOUNT_NBR"], how='left'), on=["SOURCE_ACCOUNT_NBR"], how='left')
    result_df = sixty_col_df.na.fill(0)
    for ratio in get_ratio_col_data():
        numerator, denominator = ratio.lower().split("/")
        result_df = result_df.withColumn(ratio, division_udf(result_df[numerator], result_df[denominator]))
    return result_df


def calculate_months_metrics(dis_ac_no_df, df, d_or_w, months):
    if months == "m1":
        tdf = dis_ac_no_df.join(df.groupBy("SOURCE_ACCOUNT_NBR").agg(
            f.count("TRANAMOUNT").alias("n" + d_or_w + months),
            f.sum("TRANAMOUNT").alias("a" + d_or_w + months),
            f.max("TRANAMOUNT").alias("max_a" + d_or_w + months)
        ), on=["SOURCE_ACCOUNT_NBR"], how='left')
    elif months == "m6":
        tdf = dis_ac_no_df.join(df.groupBy("SOURCE_ACCOUNT_NBR").agg(
            f.count("TRANAMOUNT").alias("n" + d_or_w + months),
            f.sum("TRANAMOUNT").alias("a" + d_or_w + months),
            f.max("TRANAMOUNT").alias("max_a" + d_or_w + months),
            f.expr('percentile_approx(TRANAMOUNT, 0.5)').alias("med_a" + d_or_w + months)
        ), on=["SOURCE_ACCOUNT_NBR"], how='left')
    else:
        tdf = dis_ac_no_df.join(df.groupBy("SOURCE_ACCOUNT_NBR").agg(
            f.count("TRANAMOUNT").alias("n" + d_or_w + months),
            f.sum("TRANAMOUNT").alias("a" + d_or_w + months)
        ), on=["SOURCE_ACCOUNT_NBR"], how='left')
    tdf = tdf.join(df.filter(df.TRANSACTION_TYPE.rlike("(?i)NEFT|RTGS|IMPS")).groupBy(
        "SOURCE_ACCOUNT_NBR") \
        .agg(
        f.count("TRANAMOUNT").alias("nt" + d_or_w + months),
        f.sum("TRANAMOUNT").alias("at" + d_or_w + months)
    ), on=["SOURCE_ACCOUNT_NBR"], how='left')
    if months == "m1":
        tdf = tdf.join(
            df.filter(df.TRANSACTION_TYPE.rlike("(?i)CASH")).groupBy("SOURCE_ACCOUNT_NBR") \
                .agg(
                f.count("TRANAMOUNT").alias("nc" + d_or_w + months),
                f.sum("TRANAMOUNT").alias("ac" + d_or_w + months),
                f.max("TRANAMOUNT").alias("max_ac" + d_or_w + months)
            ), on=["SOURCE_ACCOUNT_NBR"], how='left')
    elif months == "m6":
        tdf = tdf.join(
            df.filter(df.TRANSACTION_TYPE.rlike("(?i)CASH")).groupBy("SOURCE_ACCOUNT_NBR") \
                .agg(
                f.count("TRANAMOUNT").alias("nc" + d_or_w + months),
                f.sum("TRANAMOUNT").alias("ac" + d_or_w + months),
                f.max("TRANAMOUNT").alias("max_ac" + d_or_w + months),
                f.expr('percentile_approx(TRANAMOUNT, 0.5)').alias("med_ac" + d_or_w + months)
            ), on=["SOURCE_ACCOUNT_NBR"], how='left')
    else:
        tdf = tdf.join(
            df.filter(df.TRANSACTION_TYPE.rlike("(?i)CASH")).groupBy("SOURCE_ACCOUNT_NBR") \
                .agg(
                f.count("TRANAMOUNT").alias("nc" + d_or_w + months),
                f.sum("TRANAMOUNT").alias("ac" + d_or_w + months)
            ), on=["SOURCE_ACCOUNT_NBR"], how='left')
    tdf = tdf.join(
        df.filter(df.TRANSACTION_TYPE.rlike("(?i)FOREX")).groupBy("SOURCE_ACCOUNT_NBR") \
            .agg(
            f.count("TRANAMOUNT").alias("nf" + d_or_w + months),
            f.sum("TRANAMOUNT").alias("af" + d_or_w + months)
        ), on=["SOURCE_ACCOUNT_NBR"], how='left')
    return tdf


spike_metrics = [
    "Med_ADM6/Max_ADM6",
    "Med_ACDM6/Max_ACDM6",  # consider median
    "Med_AWM6/Max_AWM6",
    "Med_ACWM6/Max_ACWM6",
    "Med_ADM6/Max_ADM1",
    "Med_ACDM6/Max_ACDM1",
    "Med_AWM6/Max_AWM1",
    "Med_ACWM6/Max_ACWM1",
    "Max_ADM1/Max_ADM6",
    "Max_ACDM1/Max_ACDM6",
    "Max_AWM1/Max_AWM6",
    "Max_ACWM1/Max_ACWM6",
]

simple_ratios = [
                    "ACWM1/AWM1",
                    "ACWM6/AWM6",
                    "ACWM12/AWM12",
                    "ACDM1/ADM1",
                    "ACDM6/ADM6",
                    "ACDM12/ADM12",
                    "NCWM1/NWM1",
                    "NCWM6/NWM6",
                    "NCWM12/NWM12",
                    "NCDM1/NDM1",
                    "NCDM6/NDM6",
                    "NCDM12/NDM12",
                    "ATWM1/AWM1",
                    "ATWM6/AWM6",
                    "ATWM12/AWM12",
                    "ATDM1/ADM1",
                    "ATDM6/ADM6",
                    "ATDM12/ADM12",
                    "NTWM1/NWM1",
                    "NTWM6/NWM6",
                    "NTWM12/NWM12",
                    "NTDM1/NDM1",
                    "NTDM6/NDM6",
                    "NTDM12/NDM12",
                    "AWM1/ADM1",
                    "ACWM1/ACDM1",
                    "ATWM1/ATDM1",
                    "AWM6/ADM6",
                    "ACWM6/ACDM6",
                    "ATWM6/ATDM6",
                    "ACDM1/ACDM6",
                    "ACDM6/ACDM12",
                    "ADM1/ADM6",
                    "ADM6/ADM12",
                    "ATDM1/ATDM6",
                    "ATDM6/ATDM12",
                    "ACWM1/ACWM6",
                    "ACWM6/ACWM12",
                    "AWM1/AWM6",
                    "AWM6/AWM12",
                    "ATWM1/ATWM6",
                    "ATWM6/ATWM12",
                    "NCDM1/NCDM6",
                    "NCDM6/NCDM12",
                    "NDM1/NDM6",
                    "NDM6/NDM12",
                    "NTDM1/NTDM6",
                    "NTDM6/NTDM12",
                    "NCWM1/NCWM6",
                    "NCWM6/NCWM12",
                    "NWM1/NWM6",
                    "NWM6/NWM12",
                    "NTWM1/NTWM6",
                    "NTWM6/NTWM12",
                    "nfwm1/nfwm6",
                    "nfwm6/nfwm12",
                    "nfwm1/nwm1",
                    "nfwm6/nwm6",
                    "nfdm1/nfdm6",
                    "nfdm6/nfdm12",
                    "nfdm1/ndm1",
                    "nfdm6/ndm6",
                    "nfwm1/ndm1",
                    "nfwm6/nfdm6",
                    "afwm1/afwm6",
                    "afwm6/afwm12",
                    "afwm1/awm1",
                    "afwm6/awm6",
                    "afdm1/afdm6",
                    "afdm6/afdm12",
                    "afdm1/adm1",
                    "afdm6/adm6",
                    "afwm1/adm1",
                    "afwm6/afdm6",
                ] + spike_metrics


def get_max_tran_date(data_frame: DataFrame):
    return data_frame.agg({"TRANDATE": "max"}).collect()[0][0]


def group_acc_num_and_make_metrics(data_frame: DataFrame):
    return metrics_df(data_frame)


def split_test_train(data_frame: DataFrame, number_of_rows):
    data_frame = data_frame.withColumn("is_fraud", random_udf(data_frame.SOURCE_ACCOUNT_NBR))
    data_frame = data_frame.withColumnRenamed("SOURCE_ACCOUNT_NBR", "san")
    data_frame = data_frame.coalesce(1)
    golden_ratio = 0.1
    test_rows = int(number_of_rows * golden_ratio)
    train_rows = number_of_rows - test_rows
    test_x_df = data_frame.sample(golden_ratio)
    train_x_df = data_frame.sample(1-golden_ratio)
    test_y_df = test_x_df.select(test_x_df.san, test_x_df.is_fraud)
    train_y_df = train_x_df.select(test_x_df.san, test_x_df.is_fraud)
    def_path = data_output_path
    path = def_path + str(int(datetime.now().timestamp() * 1000))
    test_x_df.drop(test_x_df.is_fraud).write.csv(path + "/test_x", header="true")
    train_x_df.drop(test_x_df.is_fraud).write.csv(path + "/train_x", header="true")
    test_y_df.write.csv(path + "/test_y", header="true")
    train_y_df.write.csv(path + "/train_y", header="true")


alert_file_path = sys.argv[1]
transaction_file_path = sys.argv[2]
data_output_path = sys.argv[3]
spark: SparkSession = SparkSession.builder.appName("AMLX")\
    .config("spark.master", "local")\
    .config("spark.executor.memory", "70g")\
    .config("spark.driver.memory", "12g")\
    .getOrCreate()
spark.conf.set("spark.sql.crossJoin.enabled", "true")
alert_df = spark.read.csv(
    alert_file_path,
    inferSchema="false",
    header="true",
    schema=get_alert_schema(),
)
transaction_df = spark.read.csv(
    transaction_file_path,
    inferSchema="false",
    header="true",
    schema=get_transaction_schema(),
)
final_df = group_acc_num_and_make_metrics(transaction_df)
split_test_train(final_df, final_df.count())
