import os
import sys

import numpy as np
import pandas as pd
from datetime import timedelta, datetime
from functools import partial

from pyspark.sql import SparkSession

from utils import *


def metrics_df(df):
    one_month_before = df.TRANDATE.max().date() - timedelta(days=30)
    six_months_before = df.TRANDATE.max().date() - timedelta(days=180)
    twelve_months_before = df.TRANDATE.max().date() - timedelta(days=365)

    one_mth_df = df[df.TRANDATE > pd.Timestamp(one_month_before)]
    one_mth_w_df = df[df.TRANDATE > pd.Timestamp(one_month_before)][
        df["PARTTRANTYPE"] == "D"
    ]
    one_mth_d_df = df[df.TRANDATE > pd.Timestamp(one_month_before)][
        df["PARTTRANTYPE"] == "C"
    ]

    six_mth_df = df[df.TRANDATE > pd.Timestamp(six_months_before)]
    six_mth_w_df = df[df.TRANDATE > pd.Timestamp(six_months_before)][
        df["PARTTRANTYPE"] == "D"
    ]
    six_mth_d_df = df[df.TRANDATE > pd.Timestamp(six_months_before)][
        df["PARTTRANTYPE"] == "C"
    ]

    twelve_mth_df = df[df.TRANDATE > pd.Timestamp(twelve_months_before)]
    twelve_mth_w_df = df[df.TRANDATE > pd.Timestamp(twelve_months_before)][
        df["PARTTRANTYPE"] == "D"
    ]
    twelve_mth_d_df = df[df.TRANDATE > pd.Timestamp(twelve_months_before)][
        df["PARTTRANTYPE"] == "C"
    ]

    grp = df.groupby("SOURCE_ACCOUNT_NBR")
    one_mth_grp = one_mth_df.groupby("SOURCE_ACCOUNT_NBR")
    one_mth_w_grp = one_mth_w_df.groupby("SOURCE_ACCOUNT_NBR")
    one_mth_d_grp = one_mth_d_df.groupby("SOURCE_ACCOUNT_NBR")
    six_mth_grp = six_mth_df.groupby("SOURCE_ACCOUNT_NBR")
    six_mth_w_grp = six_mth_w_df.groupby("SOURCE_ACCOUNT_NBR")
    six_mth_d_grp = six_mth_d_df.groupby("SOURCE_ACCOUNT_NBR")
    twelve_mth_grp = twelve_mth_df.groupby("SOURCE_ACCOUNT_NBR")
    twelve_mth_w_grp = twelve_mth_w_df.groupby("SOURCE_ACCOUNT_NBR")
    twelve_mth_d_grp = twelve_mth_d_df.groupby("SOURCE_ACCOUNT_NBR")
    # Create the dataframe to return
    newdf = pd.DataFrame(index=df.SOURCE_ACCOUNT_NBR.unique())
    newdf["san"] = newdf.index
    newdf["nwm1"] = check_and_return(
        one_mth_w_grp.apply(lambda r: r["PARTTRANTYPE"].count())
    )
    newdf["awm1"] = check_and_return(
        one_mth_w_grp.apply(lambda r: r["TRANAMOUNT"].sum())
    )
    newdf["ndm1"] = check_and_return(
        one_mth_d_grp.apply(lambda r: r["PARTTRANTYPE"].count())
    )
    newdf["adm1"] = check_and_return(
        one_mth_d_grp.apply(lambda r: r["TRANAMOUNT"].sum())
    )

    newdf["ncdm1"] = check_and_return(one_mth_d_grp.apply(num_cash_txns))
    newdf["acdm1"] = check_and_return(one_mth_d_grp.apply(amt_cash_txns))
    newdf["ncwm1"] = check_and_return(one_mth_w_grp.apply(num_cash_txns))
    newdf["acwm1"] = check_and_return(one_mth_w_grp.apply(amt_cash_txns))

    newdf["nwm6"] = check_and_return(
        six_mth_w_grp.apply(lambda r: r["PARTTRANTYPE"].count())
    )
    newdf["awm6"] = check_and_return(
        six_mth_w_grp.apply(lambda r: r["TRANAMOUNT"].sum())
    )
    newdf["ndm6"] = check_and_return(
        six_mth_d_grp.apply(lambda r: r["PARTTRANTYPE"].count())
    )
    newdf["adm6"] = check_and_return(
        six_mth_d_grp.apply(lambda r: r["TRANAMOUNT"].sum())
    )

    newdf["ncdm6"] = check_and_return(six_mth_d_grp.apply(num_cash_txns))
    newdf["acdm6"] = check_and_return(six_mth_d_grp.apply(amt_cash_txns))
    newdf["ncwm6"] = check_and_return(six_mth_w_grp.apply(num_cash_txns))
    newdf["acwm6"] = check_and_return(six_mth_w_grp.apply(amt_cash_txns))

    newdf["nwm12"] = check_and_return(
        twelve_mth_w_grp.apply(lambda r: r["PARTTRANTYPE"].count())
    )
    newdf["awm12"] = check_and_return(
        twelve_mth_w_grp.apply(lambda r: r["TRANAMOUNT"].sum())
    )
    newdf["ndm12"] = check_and_return(
        twelve_mth_d_grp.apply(lambda r: r["PARTTRANTYPE"].count())
    )
    newdf["adm12"] = check_and_return(
        twelve_mth_d_grp.apply(lambda r: r["TRANAMOUNT"].sum())
    )

    newdf["ncdm12"] = check_and_return(twelve_mth_d_grp.apply(num_cash_txns))
    newdf["acdm12"] = check_and_return(twelve_mth_d_grp.apply(amt_cash_txns))
    newdf["ncwm12"] = check_and_return(twelve_mth_w_grp.apply(num_cash_txns))
    newdf["acwm12"] = check_and_return(twelve_mth_w_grp.apply(amt_cash_txns))

    newdf["ntwm1"] = check_and_return(one_mth_w_grp.apply(num_neft_rtgs_imps_txn))
    newdf["ntwm6"] = check_and_return(six_mth_w_grp.apply(num_neft_rtgs_imps_txn))
    newdf["ntwm12"] = check_and_return(twelve_mth_w_grp.apply(num_neft_rtgs_imps_txn))
    newdf["ntdm1"] = check_and_return(one_mth_d_grp.apply(num_neft_rtgs_imps_txn))
    newdf["ntdm6"] = check_and_return(six_mth_d_grp.apply(num_neft_rtgs_imps_txn))
    newdf["ntdm12"] = check_and_return(twelve_mth_d_grp.apply(num_neft_rtgs_imps_txn))

    newdf["atwm1"] = check_and_return(one_mth_w_grp.apply(amt_neft_rtgs_imps_txn))
    newdf["atwm6"] = check_and_return(six_mth_w_grp.apply(amt_neft_rtgs_imps_txn))
    newdf["atwm12"] = check_and_return(twelve_mth_w_grp.apply(amt_neft_rtgs_imps_txn))
    newdf["atdm1"] = check_and_return(one_mth_d_grp.apply(amt_neft_rtgs_imps_txn))
    newdf["atdm6"] = check_and_return(six_mth_d_grp.apply(amt_neft_rtgs_imps_txn))
    newdf["atdm12"] = check_and_return(twelve_mth_d_grp.apply(amt_neft_rtgs_imps_txn))
    # FOREX: (a|n)f(w|d)m(1|6|12)
    newdf["nfwm1"] = check_and_return(one_mth_w_grp.apply(num_forex_txn))
    newdf["nfwm6"] = check_and_return(six_mth_w_grp.apply(num_forex_txn))
    newdf["nfwm12"] = check_and_return(twelve_mth_w_grp.apply(num_forex_txn))
    newdf["nfdm1"] = check_and_return(one_mth_d_grp.apply(num_forex_txn))
    newdf["nfdm6"] = check_and_return(six_mth_d_grp.apply(num_forex_txn))
    newdf["nfdm12"] = check_and_return(twelve_mth_d_grp.apply(num_forex_txn))

    newdf["afwm1"] = check_and_return(one_mth_w_grp.apply(amt_forex_txn))
    newdf["afwm6"] = check_and_return(six_mth_w_grp.apply(amt_forex_txn))
    newdf["afwm12"] = check_and_return(twelve_mth_w_grp.apply(amt_forex_txn))
    newdf["afdm1"] = check_and_return(one_mth_d_grp.apply(amt_forex_txn))
    newdf["afdm6"] = check_and_return(six_mth_d_grp.apply(amt_forex_txn))
    newdf["afdm12"] = check_and_return(twelve_mth_d_grp.apply(amt_forex_txn))

    newdf["med_adm6"] = check_and_return(six_mth_d_grp["TRANAMOUNT"].median())
    newdf["max_adm6"] = check_and_return(six_mth_d_grp["TRANAMOUNT"].max())
    newdf["med_acdm6"] = check_and_return(
        six_mth_d_grp.apply(
            lambda r: r[
                r.TRANSACTION_TYPE.str.contains("CASH", na=False)
            ].TRANAMOUNT.median()
        )
    )
    newdf["max_acdm6"] = check_and_return(
        six_mth_d_grp.apply(
            lambda r: r[
                r.TRANSACTION_TYPE.str.contains("CASH", na=False)
            ].TRANAMOUNT.max()
        )
    )

    newdf["med_awm6"] = check_and_return(six_mth_w_grp["TRANAMOUNT"].median())
    newdf["max_awm6"] = check_and_return(six_mth_w_grp["TRANAMOUNT"].max())
    newdf["med_acwm6"] = check_and_return(
        six_mth_w_grp.apply(
            lambda r: r[
                r.TRANSACTION_TYPE.str.contains("CASH", na=False)
            ].TRANAMOUNT.median()
        )
    )
    newdf["max_acwm6"] = check_and_return(
        six_mth_w_grp.apply(
            lambda r: r[
                r.TRANSACTION_TYPE.str.contains("CASH", na=False)
            ].TRANAMOUNT.max()
        )
    )

    newdf["max_adm1"] = check_and_return(one_mth_d_grp["TRANAMOUNT"].max())
    newdf["max_awm1"] = check_and_return(one_mth_w_grp["TRANAMOUNT"].max())
    newdf["max_acdm1"] = check_and_return(
        one_mth_d_grp.apply(
            lambda r: r[
                r.TRANSACTION_TYPE.str.contains("CASH", na=False)
            ].TRANAMOUNT.max()
        )
    )
    newdf["max_acwm1"] = check_and_return(
        one_mth_w_grp.apply(
            lambda r: r[
                r.TRANSACTION_TYPE.str.contains("CASH", na=False)
            ].TRANAMOUNT.max()
        )
    )

    newdf = newdf.fillna(0)
    for ratio in simple_ratios:
        newdf[ratio.lower()] = newdf.apply(partial(generate_ratio, ratio=ratio), axis=1)
    return newdf


def check_and_return(df):
    if len(df) > 0:
        return df
    else:
        return 0


def generate_ratio(r, ratio):
    numerator, denominator = ratio.lower().split("/")
    if r[numerator] == 0:
        return 0
    else:
        return np.float64(r[numerator]) / r[denominator]


def num_cash_txns(r):
    return r[r.TRANSACTION_TYPE.str.contains("CASH", na=False)].shape[0]


def amt_cash_txns(r):
    return r[r.TRANSACTION_TYPE.str.contains("CASH", na=False)].TRANAMOUNT.sum()


def num_neft_rtgs_imps_txn(r):
    return r[r.TRANSACTION_TYPE.str.contains("NEFT|RTGS|IMPS", case=False, na=False)][
        "PARTTRANTYPE"
    ].count()


def amt_neft_rtgs_imps_txn(r):
    return r[
        r.TRANSACTION_TYPE.str.contains("NEFT|RTGS|IMPS", case=False, na=False)
    ].TRANAMOUNT.sum()


def num_forex_txn(r):
    return r[
        np.logical_or(
            r.TRANSACTION_TYPE.str.contains("FOREX", na=False, case=False), r.REMITTANCE
        )
    ].shape[0]


def amt_forex_txn(r):
    return r[
        np.logical_or(
            r.TRANSACTION_TYPE.str.contains("FOREX", na=False, case=False), r.REMITTANCE
        )
    ].TRANAMOUNT.sum()


def get_casa_from_scheme_code(scheme_code):
    if "SB" in scheme_code or "SK" in scheme_code or "SA" in scheme_code:
        return "s"
    elif "CA" in scheme_code:
        return "c"
    else:
        return "o"


spike_metrics = [
    "Med_ADM6/Max_ADM6",
    "Med_ACDM6/Max_ACDM6",  # consider median
    "Med_AWM6/Max_AWM6",
    "Med_ACWM6/Max_ACWM6",
    "Med_ADM6/Max_ADM1",
    "Med_ACDM6/Max_ACDM1",
    "Med_AWM6/Max_AWM1",
    "Med_ACWM6/Max_ACWM1",
    "Max_ADM1/Max_ADM6",
    "Max_ACDM1/Max_ACDM6",
    "Max_AWM1/Max_AWM6",
    "Max_ACWM1/Max_ACWM6",
]

simple_ratios = [
    "ACWM1/AWM1",
    "ACWM6/AWM6",
    "ACWM12/AWM12",
    "ACDM1/ADM1",
    "ACDM6/ADM6",
    "ACDM12/ADM12",
    "NCWM1/NWM1",
    "NCWM6/NWM6",
    "NCWM12/NWM12",
    "NCDM1/NDM1",
    "NCDM6/NDM6",
    "NCDM12/NDM12",
    "ATWM1/AWM1",
    "ATWM6/AWM6",
    "ATWM12/AWM12",
    "ATDM1/ADM1",
    "ATDM6/ADM6",
    "ATDM12/ADM12",
    "NTWM1/NWM1",
    "NTWM6/NWM6",
    "NTWM12/NWM12",
    "NTDM1/NDM1",
    "NTDM6/NDM6",
    "NTDM12/NDM12",
    "AWM1/ADM1",
    "ACWM1/ACDM1",
    "ATWM1/ATDM1",
    "AWM6/ADM6",
    "ACWM6/ACDM6",
    "ATWM6/ATDM6",
    "ACDM1/ACDM6",
    "ACDM6/ACDM12",
    "ADM1/ADM6",
    "ADM6/ADM12",
    "ATDM1/ATDM6",
    "ATDM6/ATDM12",
    "ACWM1/ACWM6",
    "ACWM6/ACWM12",
    "AWM1/AWM6",
    "AWM6/AWM12",
    "ATWM1/ATWM6",
    "ATWM6/ATWM12",
    "NCDM1/NCDM6",
    "NCDM6/NCDM12",
    "NDM1/NDM6",
    "NDM6/NDM12",
    "NTDM1/NTDM6",
    "NTDM6/NTDM12",
    "NCWM1/NCWM6",
    "NCWM6/NCWM12",
    "NWM1/NWM6",
    "NWM6/NWM12",
    "NTWM1/NTWM6",
    "NTWM6/NTWM12",
    "nfwm1/nfwm6",
    "nfwm6/nfwm12",
    "nfwm1/nwm1",
    "nfwm6/nwm6",
    "nfdm1/nfdm6",
    "nfdm6/nfdm12",
    "nfdm1/ndm1",
    "nfdm6/ndm6",
    "nfwm1/ndm1",
    "nfwm6/nfdm6",
    "afwm1/afwm6",
    "afwm6/afwm12",
    "afwm1/awm1",
    "afwm6/awm6",
    "afdm1/afdm6",
    "afdm6/afdm12",
    "afdm1/adm1",
    "afdm6/adm6",
    "afwm1/adm1",
    "afwm6/afdm6",
] + spike_metrics

drop_list = [
    "BACID",
    "FLOW_ID",
    "CURRENCYCODE",
    "RISK_SCORE",
    "TAX_DIVIDEND",
    "INTERNAL_EXTERNAL",
    "FLAG_C2",
    "TREASURYREFNO",
    "FLAG_100CR",
    "RATECODE",
    "INSTRUMENTALPHA",
    "EBOR_FLG",
    "INSTRUMENTDT",
    "INTRUMENTTYPE_y",
    "VALUE_RANGE",
    "RPTCODE",
    "INSTRUMENTNUM",
    "TRANREMARK_x",
    "TRANREMARK_y",
    "TRAN_PARTICULAR_2",
    "ACCOUNTID",
    "COMPANY_NAME",
    "BANK_NAME",
    "DELIVERY_CHANNEL_ID_y",
    "INITSOLID_y",
    "MODULEID_y",
    "SOLID_y",
    "DD_PO",
    "OTHER",
    "GLSUB",
    "ANAME",
    "VOUCHPRINTID",
    "ENTRYUSERID",
    "POSTEDUSERID",
    "POSTEDDT",
    "VERFIEDUSERID",
    "TRANPARTICULAR_y",
    "TRANTYPE_y",
    "TRAN_START_DATE_y",
    "CUSTOMER_INDUCED_y",
    "CREATEDUSERID",
    "LASTCHANGEUSERID",
    "NAVIGATIONFLG",
    "VERIFIEDDT",
    "CUST_NCA_ERV",
    "RESERVATIONAMT",
    "RATE",
    "RTL_NONRTL",
    "POSTEDFLG",
    "FOREXTRANAMOUNT",
    "QAB_CHARGES",
    "CHEQEUE_BOUNCE_TRXN",
    "ADVICEPRINTINDICATOR",
    "TREASURYRATE",
    "BANKCODE",
]


def trim_data_frame(data_frame):
    data_frame.drop(
        [d.rstrip("_x") for d in drop_list], axis=1, inplace=True, errors="ignore"
    )
    data_frame = data_frame[data_frame["DELFLG"] == "N"]
    data_frame.drop(["DELFLG"], axis=1, inplace=True, errors="ignore")
    data_frame = data_frame.loc[:, ~data_frame.columns.str.contains("^Unnamed")]
    data_frame["TRANDATE"] = pd.to_datetime(data_frame["TRANDATE"])
    return data_frame


def split_test_train(data_frame: pd.DataFrame, number_of_rows):
    golden_ratio = 0.1
    test_rows = int(number_of_rows * golden_ratio)
    train_rows = number_of_rows - test_rows
    test_df = data_frame.head(test_rows)
    train_df = data_frame.tail(train_rows)
    def_path = data_output_path
    path = def_path + str(int(datetime.now().timestamp() * 1000))
    os.mkdir(path)
    test_df.to_csv(path + "/test_x.csv")
    train_df.to_csv(path + "/train_x.csv")


def get_account_list_df(data_frame):
    acc_list = data_frame.SOURCE_ACCOUNT_NBR.unique()
    acc_length = len(acc_list)
    print("Account length = ", acc_length)
    temp_df = pd.DataFrame()
    for acc_no in acc_list:
        alert_list = alert_df.toPandas()
        tran_acc_data = data_frame[data_frame.SOURCE_ACCOUNT_NBR == acc_no]
        alert_acc_data = alert_list[alert_list.ACCOUNTNO == acc_no]
        temp_df = temp_df.append(metrics_df(tran_acc_data))
    # temp_df.to_csv('/Users/innovation/PycharmProjects/Amlx/out_data.csv')
    split_test_train(temp_df, acc_length)


spark: SparkSession = SparkSession.builder.appName("AMLX").config(
    "spark.master", "local"
).getOrCreate()
alert_file_path = sys.argv[1]
transaction_file_path = sys.argv[2]
data_output_path = sys.argv[3]
alert_df = spark.read.csv(
    alert_file_path, inferSchema="false", header="true", schema=get_alert_schema()
)
transaction_df = spark.read.csv(
    transaction_file_path,
    inferSchema="false",
    header="true",
    schema=get_transaction_schema(),
)
transaction_df_pd = transaction_df.toPandas()
transaction_df_pd["TRANDATE"] = pd.to_datetime(transaction_df_pd["TRANDATE"])
get_account_list_df(transaction_df_pd)

# s3://aml-tran-alert-data/AML_Data_Ver_1/AlertData_2606.csv
# s3://aml-tran-alert-data/AML_Data_Ver_1/TranData_2606.csv
# s3://amlx-stories-script-and-test-train-data/stories/
